---
title: "Work in Hesiod's Works and Days"
date: 2018-10-28T20:13:01-07:00
draft: false
---
When I turned 20, I remember saying I was about to begin the most grueling decade of my life, referring to the ascetic rigors of the PhD and tenure-track job search.  At twenty-nine I’m still very focused on what I think of as “my work”, although that turned out not to mean math research, and I’m still figuring out what exactly it does mean.  I find meaning in an eclectic array of activities: writing fiction, reading literature and philosophy, political activism, thinking about math and AI, and that sort of meaning still feels like the most important thing in my life.

Living in San Francisco means being immersed in the discourse of work.  On the one hand, there’s the rhetoric of entrepreneurship, the “hustle evangelism” so familiar by now and so easy to mock.  On the other hand, the creatives and bohemians living five to a bedroom or ensconced in their parents’ Victorians live an equally earnest kind of hustle, trying to paint or write their way up or out.  And among the growing leftist and Marxist crowd, there’s also a focus on work, whether in the form of veneration of the working class (and disputes over its membership) or daydreams about the glory of our labor returned to us, delivered from the alienating metamorphoses to which capital subjects it.  I occupy some tenuous position at the intersection of these three worlds.

In this post, I’ll unpack the ancient Greek author Hesiod’s attitudes on labor and work.  Hesiod probably wrote in the 700s BCE.  We have two texts from him, the Theogony, on the origins of the gods, and Works and Days, a grab-bag didactic poem, on agriculture, commerce, society, justice, and so on.  Hesiod is a fun and idiosyncratic author.  Where Homer can feel polished and impersonal, Hesiod’s poetry is rough around the edges but brimming with personality. 

The text of Works and Days is structured as a letter to Hesiod’s fuck-up brother Perses, giving him advice on how to manage his affairs and become wealthy.  The backstory on these two brothers is contained in two brief passages, one of which is slipped in near the poem’s end (all English quotations from the Loeb edition, trans. Glenn W. Most):

“Let us decide our quarrel right here with straight judgements, which comes from Zeus, the best ones.  For already we had divided up our allotment, but you snatched much more besides and went carrying it off, greatly honoring the kings, those gift-eaters, who want to pass this judgment—fools…” (35-40)

“You yourself wait until the sailing season arrives, and then drag your swift boat down to the sea, arrange the cargo in it and get it ready so that you can bring the profit home, just as my father and yours, Perses, you great fool, used to sail in boats, deprived as he was of a fine means of life.  Once he came here too, after he had crossed over a big sea, leaving behind Aeolian Cyme in a black boat, fleeing not wealth nor riches nor prosperity, but evil poverty, which Zeus gives to men.  And he settled near Helicon in a wretched village, Ascra, evil in winter, distressful in summer, not ever fine.” (630-640)

This family history raises more questions than answers.  If Perses double-crossed Hesiod and squandered their inheritance, why is Hesiod still looking out for Perses and giving him advice?  But it gives us some important clues about Hesiod’s mindset.  First, he’s a second-generation immigrant, who was born into poverty.  Second, he feels a sense of needless loss—the wealth that his father was eventually able to gather in Helicon was squandered needlessly.  Third, that money was wasted on kings—“gift-eaters” who were happy to accept favors, but not to reciprocate.

Hesiod is a sort of rugged self-reliant farmer, bitterly skeptical of society.  Far from meekness or modesty, he’s immensely proud, even swaggering.  He has an appreciation for wealth, and is keenly mindful of competition to build wealth.  This comes out clearly near the beginning of the poem:

“[Strife] rouses even the helpless man to work.  For a man who is not working but who looks at some other man, a rich one who is hastening to plow and plant and set his house in order, he envies him, one neighbor envying his neighbor who is hastening towards wealth, and this Strife is good for mortals.  And potter is angry with potter, and builder with builder, and beggar begrudges beggar, and poet poet.”

This is a rather bleak social picture.  Unlike Adam Smith’s invisible hand, which guides the individual to contribute to society through his own self-interest, Hesiod’s Strife guides the individual to work for his own self-interest, out of pure mulishness and spite towards his neighbors.  And for Hesiod, this is the way things should be.  Hesiod exhorts Perses to channel his greed into a will to work:

“If the spirit in your breast longs for wealth, then act in this way, and work at work upon work.” (381)

Hesiod spits this line with a familiar sadistic relish.  This is rhetoric that would be equally at home in Ben Franklin’s journal, an early twentieth-century Protestant sermon, or a billboard in twenty-first century San Francisco.  But we see how Hesiod came to this point: bitterness, betrayal, felt loss.  For Hesiod, capital is everything in life: “Property is life for worthless mortals” (687), but accumulating it is bitterly difficult:

“Misery is there to be grabbed in abundance, easily, for smooth is the road, and she lives very nearby; but in front of Excellence the immortal gods have set sweat, and the path to her is long and steep…”

This difficulty is part of the human condition for Hesiod, and it is to underscore this that he spends a sizable chunk of Works and Days on the myth of Pandora’s box (47-105) and the myth of the five ages (106-201), according to which men are now more wretched than they ever were:

“For now the race is indeed one of iron.  And they will not cease from toil and distress by day, nor from being worn out by suffering at night, and the gods will give them grievous cares.” (176-179)

Hesiod puts his frustration most starkly in a line reminiscent of Heraclitus’ “nature loves to hide”:

“The gods keep the means of life concealed from human beings.  Otherwise you would easily be able to work in just one day so as to have enough for a whole year even without working….” (42-44)

Surely Hesiod was thinking of his inheritance here, which he’d mentioned just a few lines earlier—so easily thrown away, so slow and painstaking to scrape together.

Hesiod’s austerity is not very well captured by the poem’s conclusion, which feels a bit pat: “Happy and blessed is he who knows all these things and does his work without giving offense to the immortals, distinguishing the birds and avoiding trespasses.” (825-829)  Happiness is not Hesiod’s game.  For Hesiod, labor is neither penance, nor intimate knowledge, nor quite ecstasy.  It is pain, delicious pain, the bitterly difficult “means of life,” to be drained to the dregs.
